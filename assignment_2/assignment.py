import rdflib
import pandas as pd

from utils.utils import *


def test_rule_for_predicate(g, predicate, query):
    print(f"Testing for predicate {predicate}.")
    print(f"Query is: {query}")

    pred_before = count_predicates(g, predicate)
    measure_quality(g, query)

    print("Running query")
    run_query(g, query)
    pred_after = count_predicates(g, predicate)

    print(f"Number of predicates {predicate}  BEFORE query is: {pred_before}")
    print(f"Number of predicates {predicate}  AFTER query is: {pred_after}")

    return


def task_1():
    g = rdflib.Graph()
    g.parse("./train.nt") #load local rdf file

    '''
        Available predicates:
        1. memberOf (author memberOf affiliation)
        2. hasCreator (paper creator author)
        3. appearsInConferenceSeries (paper appearsInConferenceSeries conference)
        4. hasDiscipline  (paper hasDiscipline domain)
        5. hasCoauthor (author coauthor author)
        6. cites (paper cites paper)
        7. a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>
    '''

    '''
        1. memberOf (author memberOf affiliation)

                memberOf(aut, aff) <= hasCoauthor(aut, coaut) ^ memberOf(coaut, aff)

                query = """
                    INSERT {
                        ?aut memberOf: ?aff .
                    }
                    WHERE{
                        ?coaut hasCoauthor: ?aut .
                        ?coaut memberOf: ?aff .
                    }
                """
                test_rule_for_predicate(g, 'memberOf', query)

                -> results in 30.5% confidence..... (changing the order of coaut and aut doesn't change confidence by much, taking the union as well...)

        2. hasCreator (paper creator author)

            Idea #1. Look for coauthors of the same paper
            ----------------------------------------------
            hasCreator(pap, aut) <= hasCoauthor(coaut, aut) ^ hasCreator(pap, coaut)

            query = """
                INSERT {
                    ?pap hasCreator: ?aut .
                }
                WHERE{
                    ?coaut hasCoauthor: ?aut .
                    ?pap hasCreator: ?coaut .
                }
            """         
            test_rule_for_predicate(g, 'hasCreator', query)
            -> results in low confidence 0.21 -> Probably can't be better....


        3. appearsInConferenceSeries (paper appearsInConferenceSeries conference)

            appearsInConferenceSeries(pap, conf) <= cites(pap, pap2) ^ appearsInConferenceSeries(pap2, conf)

            query = """
                INSERT {
                    ?pap appearsInConferenceSeries: ?conf .
                }
                WHERE{
                    ?pap cites: ?pap2 .
                    ?pap2 appearsInConferenceSeries: ?conf .
                }
            """         
            test_rule_for_predicate(g, 'appearsInConferenceSeries', query)
            -> results in low confidence 0.32 -> switching pap and pap2 order doesn't lowers perf....
        
        4. hasDiscipline  (paper hasDiscipline domain)
            
            Idea #1. Look for other papers of the same author and see in which domain they are and look if they were published in the same conference
            ----------------------------------------------
            hasDiscipline(pap, dom) <= hasCreator(pap, aut) ^ hasCreator(pap2, aut) ^ hasDiscipline(pap2, dom)  
                                        ^ appearsInConferenceSeries(pap, conf) ^ appearsInConferenceSeries(pap2, conf)

            query = """
                INSERT {
                    ?pap hasDiscipline: ?dom .
                }
                WHERE{
                    ?pap hasCreator: ?aut .
                    ?pap2 hasCreator: ?aut .
                    ?pap2 hasDiscipline: ?dom .
                    ?pap appearsInConferenceSeries: ?conf .
                    ?pap2 appearsInConferenceSeries: ?conf .
                }
            """         
            test_rule_for_predicate(g, 'hasDiscipline', query)
            -> results in confidence 0.42% 

            Another idea is to look for cited papers and their domain -> so """
                INSERT {
                    ?pap hasDiscipline: ?dom .
                }
                WHERE{hasCoauthor
                    ?pap cites: ?pap2 .
                    ?pap2 hasDiscipline: ?dom .
                }
            """      
            but this results in a lower confidence score

               
        5. hasCoauthor (author coauthor author) 
        
            query="""
                INSERT {
                    ?aut1 hasCoauthor: ?aut2 .
                }
                WHERE {
                    ?pap hasCreator: ?aut1 .
                    ?pap hasCreator: ?aut2 .
                }
            """
            test_rule_for_predicate(g, 'hasCoauthor', query)
            -> results in a high confidence score of 0.744 

            Another idea is to just look for coauthors that have the author listed
            query="""
                INSERT {
                    ?aut1 hasCoauthor: ?aut2 .
                }
                WHERE {
                    ?aut2 hasCoauthor: ?aut1 .
                }
            """
            -> this results in 0.98 -> so almost perfect



        6. a <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>
            query="""
                INSERT {
                    ?pap1 a ?t .
                }
                WHERE {
                    ?pap1 cites: ?pap2 .
                    ?pap2 a ?t .
                }
            """
            -> results in confidence score of 1.0 -> it is certain, the same would probably hold for other types (coauthor etc...)

    '''


    query = """
        INSERT {
            ?pap hasCreator: ?aut .
        }
        WHERE{
            ?coaut hasCoauthor: ?aut .
            ?pap hasCreator: ?coaut .
        }
    """         
    test_rule_for_predicate(g, 'hasCreator', query)

def task_2():
    g = rdflib.Graph()
    g.parse("./train.nt")

    rules_df = pd.read_csv('task_2_mined_rules_AIME.csv')
    sorted_df = rules_df.sort_values(['Std Confidence', 'PCA Confidence'], ascending=[False, False])
    sorted_rules = sorted_df["Rule"].values

    query = create_aime_rule_insert_query(sorted_rules[4]) 
    print(query)

    qres = g.query(query)
    print(qres)
    for row in qres:
        print(row)



def generate_predictions(g, rule_query):
    sparql = rule_query
    print(sparql)

    qres= g.query(sparql)
    
    results = []
    for row in qres:
        results.append((str(row[0]),str(row[1]),str(row[2])))
    return pd.DataFrame(results, columns=['Subject','Predicate','Object'])



def task_3():
    g = rdflib.Graph()
    g.parse("./train.nt")

    '''
        CONSTRUCT {Head relation}

            WHERE {

            Body relation

            MINUS {Head Relation}

        }
        -----------------------------------

        1. memberOf
        1675	772	0.4608955223880597	<http://www.w3.org/ns/org#memberOf>(X,Y) <= <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(A,X), <http://www.w3.org/ns/org#memberOf>(A,Y)

        query = """CONSTRUCT { ?x  <http://www.w3.org/ns/org#memberOf>  ?y }
            where {
            ?a <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor> ?x . 
            ?a <http://www.w3.org/ns/org#memberOf> ?y 
            MINUS { ?x  <http://www.w3.org/ns/org#memberOf>  ?y }

            }"""

        -----------------------------------

        2. appearsInConferenceSeries
        1708	682	0.3992974238875878	<https://makg.org/property/appearsInConferenceSeries>(X,Y) <= <http://purl.org/spar/cito/cites>(X,A), <https://makg.org/property/appearsInConferenceSeries>(A,Y)

        query = """CONSTRUCT { ?x  <https://makg.org/property/appearsInConferenceSeries>  ?y }
            where {
            ?x <http://purl.org/spar/cito/cites> ?a .
            ?a  <https://makg.org/property/appearsInConferenceSeries> ?y

            MINUS { ?x  <https://makg.org/property/appearsInConferenceSeries>  ?y }

            }"""

        -----------------------------------

        3.  hasCreator
        2000	1321	0.6605	<http://purl.org/dc/terms/creator>(X,Y) <= <http://purl.org/dc/terms/creator>(X,A), <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(A,Y)

        query = """CONSTRUCT { ?x  <http://purl.org/dc/terms/creator>  ?y }
            where {
            ?x <http://purl.org/spar/cito/cites> ?a .
            ?a  <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor> ?y

            MINUS { ?x  <http://purl.org/dc/terms/creator>  ?y }

            }"""

        query = 
            
        4. hasCoauthor
        -----------------------------------

        query = """CONSTRUCT { ?x  <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>  ?y }
            where {
            ?y <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor> ?x 

            MINUS { ?x  <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>  ?y }

        }"""

        -----------------------------------

        5. hasDiscipline

        query = """CONSTRUCT { ?x  <http://purl.org/spar/fabio/hasDiscipline>  ?y }
            where {
            ?x <http://purl.org/dc/terms/creator> ?a .
            ?b <http://purl.org/dc/terms/creator> ?a .
            ?b <http://purl.org/spar/fabio/hasDiscipline> ?y .
            ?x <https://makg.org/property/appearsInConferenceSeries> ?c .
            ?b <https://makg.org/property/appearsInConferenceSeries> ?c 

            MINUS { ?x  <http://purl.org/spar/fabio/hasDiscipline>  ?y }

        }"""

            INSERT {
                ?x hasDiscipline: ?y .
            }
            WHERE{
                ?x hasCreator: ?a .
                ?b hasCreator: ?a .
                ?b hasDiscipline: ?y .
                ?x appearsInConferenceSeries: ?c .
                ?b appearsInConferenceSeries: ?c .
            }


    
    '''

    query = """CONSTRUCT { ?x  <http://www.w3.org/ns/org#memberOf>  ?y }
        where {
        ?a <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor> ?x . 
        ?a <http://www.w3.org/ns/org#memberOf> ?y 
        MINUS { ?x  <http://www.w3.org/ns/org#memberOf>  ?y }

        }"""


    predictions= generate_predictions(g,query)
    i = 0
    for p in predictions:
        print(p)
        i = i + 1
        if i == 11:
            break



if __name__ == "__main__":
    #task_1()
    #task_2()
    task_3()
