Task 2
---------------------------------

 We applied rule mining on amie_plus (we had to replace space characters in .nt to be able to do this...)
# we ended up with 19 rules 


Only the first 10 have a confidence score of more than 30%....
the first four have confidence of 1.0 -> these are for type predicate (author, paper)

5/6 are for the coauthor that we already discovered previously with confidence of 98.9% and 0.74%
7th/8th is for appearsInConferenceSeries with 32.6% and 30.9%

9th and 10th are for memberOf with 30.5%




So let's try with AnyBURL as well.......

java -Xmx12G -cp AnyBURL-JUNO.jar de.unima.ki.anyburl.LearnReinforced config-learn.properties


It fouind 112844 rules that it saved to rules-50 

We trained again but this time with a threshold_confidence of 0.3/0.6 to decrease the number of rules in the files. We can also increase the SNAPSHOTS_AT parameter to 
get better results. Now we found 32902 rules .

We found the following rules among the learned rules from different training sessions.


Lots of type rules that we already knew before

TYPE
2000	2000	1.0	<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>(X,Y) <= <https://makg.org/property/appearsInConferenceSeries>(X,A), <https://makg.org/property/appearsInConferenceSeries>(B,A), <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>(B,Y)


MEMBEROF
1876	859	0.4578891257995736	<http://www.w3.org/ns/org#memberOf>(X,Y) <= <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(X,A), <http://www.w3.org/ns/org#memberOf>(A,Y)
1675	772	0.4608955223880597	<http://www.w3.org/ns/org#memberOf>(X,Y) <= <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(A,X), <http://www.w3.org/ns/org#memberOf>(A,Y)


coauthor
2000	1291	0.6455	<http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(X,Y) <= <http://purl.org/dc/terms/creator>(A,X), <http://purl.org/dc/terms/creator>(A,B), <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(B,Y)
2000	1977	0.9885	<http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(X,Y) <= <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(Y,X)

creator
2000	1321	0.6605	<http://purl.org/dc/terms/creator>(X,Y) <= <http://purl.org/dc/terms/creator>(X,A), <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(A,Y)
2000	1314	0.657	<http://purl.org/dc/terms/creator>(X,Y) <= <http://purl.org/dc/terms/creator>(X,A), <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>(Y,A)


appearsInConferenceSeries
1708	682	0.3992974238875878	<https://makg.org/property/appearsInConferenceSeries>(X,Y) <= <http://purl.org/spar/cito/cites>(X,A), <https://makg.org/property/appearsInConferenceSeries>(A,Y)
1918	719	0.3748696558915537	<https://makg.org/property/appearsInConferenceSeries>(X,Y) <= <http://purl.org/spar/cito/cites>(A,X), <https://makg.org/property/appearsInConferenceSeries>(A,Y)



WE didnt find any general rule for cites or hasDiscipline only specific rules for specific entities. For example

7	3	0.42857142857142855	<http://purl.org/spar/cito/cites>(<http://MAGexample.org/80AD06E8>,Y) <= <http://purl.org/spar/cito/cites>(<http://MAGexample.org/7760660C>,Y)



Task 3
-----------------------------------------