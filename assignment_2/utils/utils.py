import re

def build_prefixes(q):
    '''
        Function that add all necessary prefixes to the string and returns it
    '''
    new_query = f"""
        PREFIX memberOf: <http://www.w3.org/ns/org#memberOf>
        PREFIX hasCreator: <http://purl.org/dc/terms/creator>
        PREFIX appearsInConferenceSeries: <https://makg.org/property/appearsInConferenceSeries>
        PREFIX hasDiscipline: <http://purl.org/spar/fabio/hasDiscipline>
        PREFIX hasCoauthor: <http://lsdis.cs.uga.edu/projects/semdis/opus#coauthor>
        PREFIX cites: <http://purl.org/spar/cito/cites>
        PREFIX a: <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>
        {q}
    """
    return new_query

def run_query(g, q):
    '''
        Function runs either 'update' or 'query' methods on the graph and returns the result
    '''
    query = build_prefixes(q)
    qres = -1 # update method already returns None....
    if 'INSERT' in query:
        qres = g.update(query)
    else:
        qres = g.query(query)
    return qres

def count_predicates(g, p):
    q = """
        SELECT *
        WHERE {
            ?x predicate: ?y .
        }      
        """
    q = q.replace("predicate", p)
    qres = run_query(g,q)
    return len(qres)


def build_support_body_queries(query):
    '''
        Function that builds queries for support and body parts of the query 
        -> LOOK AT SLIDES IF THIS VARIATION OF SUPPORT/BODY Queries is okay.. 
        I took the first variation (Confidence Under Closed-World Assumption), 
        but there exists 2 more on the slides (PCA and generalized...) 
    '''
    # Find all instances of ? and : characters (first two ? are unnecessary, but better for debugging...)
    reg = '\?([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)'
    objects = re.findall(reg, query)

    # Don't know how to get this one with regex....
    predicates = query.split()
    predicates = [p for p in predicates if p.endswith(':')]

    # Build the base query with first two objects
    base_query = """
        SELECT * {
        SELECT DISTINCT ?obj1 ?obj2
        WHERE{
            {where}
        }}
    """.replace("obj1",objects[0]).replace("obj2",objects[1])

    supp_where = ""
    body_where = ""

    # Now build the 'where' for the support and body queries.
    i = 0
    for p in predicates:   
        supp_where += f"?{objects[i*2]} {p} ?{objects[i*2 + 1]} .\n"

        if i == 0:
            i = 1
            continue

        body_where += f"?{objects[i*2]} {p} ?{objects[i*2 + 1]} .\n"
        i = i + 1    

    supp_query = base_query.replace("{where}", supp_where)
    body_query = base_query.replace("{where}", body_where)

    return supp_query, body_query

def measure_quality(g, query):
    supp_query, body_query = build_support_body_queries(query)

    r1 = run_query(g, supp_query)
    r2 = run_query(g, body_query)
    
    print("Support query: ")
    print(supp_query)
    print("Body query")
    print(body_query)
    print("----------------")
    print(f"Num of support query results: {len(r1)}")
    print(f"Num of body query results: {len(r2)}")

    conf = len(r1)/len(r2)
    print(f"Confidence score: {conf}")
    return conf



''' Task 2 '''

from dataclasses import replace
def parse_rule_aime_format(a_rule):
    body, head = a_rule.split(' => ')
    head_sub, head_rel, head_obj= head.split('  ')
    head_sub = head_sub.replace('?','')
    head_obj = head_obj.replace('?','')
    body= body.split('  ')
    body =" ".join(body[:3])+' . \n '+" ".join(body[-4:]) + '.'
    rule ={'head':head, 'body':body, 'relation':head_rel, \
                                     'head_subj':head_sub,  'head_obj':head_obj}
   
    return rule

def construct_query_for_aime_rule(arule):
    rule = {key: value[:] for key, value in arule.items()}
  
    triple_pattern = ""
    triple_pattern = """SELECT DISTINCT ?{sub} ?{obj} 
      
    """.format(sub = rule['head_subj'], obj = rule['head_obj'])+ "where {  " + triple_pattern
    
    triple_pattern+=  rule['body']
    triple_pattern+= "MINUS {"+rule['head']+" .}"
    triple_pattern+= """
    }"""
    
    return triple_pattern

def create_aime_rule_insert_query(a_rule):
        parsed_rule = parse_rule_aime_format(a_rule)

        query = construct_query_for_aime_rule(parsed_rule)

        return query