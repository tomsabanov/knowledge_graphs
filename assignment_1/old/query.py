import kglab
import pandas as pd

namespaces = {
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "scm": "https://schema.org/",
    "ex": "https://example.org/"
}

kg = kglab.KnowledgeGraph(
    name = "AlbumsArtists",
    base_uri = "http://www.example.org/artist/",
    namespaces = namespaces,
)

kg.load_rdf("triples.ttl")


sparql ="""
        SELECT ?albumsales ?MusicAlbum
        WHERE{
            ?MusicAlbum scm:genre ?genre .
            ?MusicAlbum scm:datePublished ?datePublished .
            ?MusicAlbum ex:albumsales ?albumsales .
            FILTER(?genre = "Country") .
            FILTER(year(?datePublished) = 2012) .
        }
        ORDER BY desc(?albumsales)
  """


df = kg.query_as_df(sparql)
print(df.head(20))

#First Question
"""
        SELECT *
        WHERE {
        ?MusicAlbum rdf:type scm:MusicAlbum .
        ?MusicAlbum ex:aggregateRating ?aggregateRating .
        FILTER(?aggregateRating >= 4.5) .
  }
  """ # Returns Music albums count = 20027

#Second Question
"""
        SELECT (SUM(?albumsales) AS ?total_album_sales_Netherlands)
        WHERE{
            ?Person scm:nationality ?Nationality .
            ?MusicAlbum scm:byArtist ?Person .
            ?MusicAlbum ex:albumsales ?albumsales .
            FILTER(?Nationality = "Netherlands") .
        }
  """ ## Returns 223264202

#Third Question
"""
        SELECT (COUNT(?MusicAlbum) AS ?count_album_country_2012)
        WHERE{
            ?MusicAlbum scm:genre ?genre .
            ?MusicAlbum scm:datePublished ?datePublished .
            FILTER(?genre = "Country") .
            FILTER(year(?datePublished) = 2012) .
        }
  """ ### Returns 90

#Third Question Part 2

"""
        SELECT ?albumsales ?MusicAlbum
        WHERE{
            ?MusicAlbum scm:genre ?genre .
            ?MusicAlbum scm:datePublished ?datePublished .
            ?MusicAlbum ex:albumsales ?albumsales .
            FILTER(?genre = "Country") .
            FILTER(year(?datePublished) = 2012) .
        }
        ORDER BY desc(?albumsales)
  """ ## Returns: 997630  <http://www.example.org/album/99511> ##