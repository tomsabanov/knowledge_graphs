from rdflib import URIRef, BNode, Literal, Namespace
from rdflib.namespace import XSD, RDF, RDFS
from rdflib import Graph
import rdflib
import kglab
import pandas as pd

namespaces = {
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "scm": "https://schema.org/",
    "ex": "https://example.org/"
}

kg = kglab.KnowledgeGraph(
    name = "AlbumsArtists",
    base_uri = "http://www.example.org/artist/",
    namespaces = namespaces,
)

df_art = pd.read_csv("../artists.csv")
df_alb = pd.read_csv("../albums.csv")

# Maps for holding artist/album nodes  
artists = {}
albums = {}


###### Add individuals and their types + properties

# Artists
for index, row in df_art.iterrows():
    id = row["id"]
    country = row["country"]
    node = rdflib.URIRef("https://example.org/Artist/{}".format(id))

    # Add artist as type Person
    kg.add(node, kg.get_ns("rdf").type, kg.get_ns("scm").Person)

    # Add nationality property
    kg.add(node, kg.get_ns("scm").nationality, rdflib.Literal(country))

    # Save node
    artists[id] = node


# Albums
for index, row in df_alb.iterrows():
    album_id = row["id"]
    artist_id = row["artist_id"]
    year = row["year_of_pub"]
    genre = row["genre"]
    num_sales = row["num_of_sales"]
    critic = row["mtv_critic"]

    node = rdflib.URIRef("http://www.example.org/Music_Album/{}".format(album_id))

    # Add album as type MusicAlbum
    kg.add(node, kg.get_ns("rdf").type, kg.get_ns("scm").MusicAlbum)

    # Add properties
    date = str(year) + "-01-01"
    kg.add(node, kg.get_ns("scm").datePublished, rdflib.Literal(date, datatype=XSD['date']))
    kg.add(node, kg.get_ns("scm").genre, rdflib.Literal(genre))
    kg.add(node, kg.get_ns("scm").aggregateRating, rdflib.Literal(critic, datatype=XSD.integer))
    kg.add(node, kg.get_ns("ex").albumsales, rdflib.Literal(num_sales, datatype=XSD.integer))

    #### Add relation property => MusicAlbum by artist 
    kg.add(node, kg.get_ns("scm").byArtist, artists[artist_id])

    # Save node
    albums[album_id] = node



s = kg.save_rdf_text(format="ttl")
kg.save_rdf("ass1.ttl")



