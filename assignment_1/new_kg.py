from rdflib import URIRef, BNode, Literal, Namespace
from rdflib.namespace import XSD, RDF, RDFS
from rdflib import Graph
import rdflib
import kglab
import pandas as pd

namespaces = {
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "scm": "https://schema.org/",
    "ex": "https://example.org/"
}

kg = kglab.KnowledgeGraph(
    name = "AlbumsArtists",
    base_uri = "http://www.example.org/artist/",
    namespaces = namespaces,
)


df_art = pd.read_csv("../artists.csv")
df_alb = pd.read_csv("../albums.csv")


# Maps for holding artist/album/genres/country nodes  
artists = {}
countries = {}

albums = {}
genres = {}


def add_artists_countries():
    # We add the artists and countries to the graph

    # Unique URI for Person https://example.org/Person/id
    # Unique URI for Country https://example.org/Country/country_name

    # Artists
    for index, row in df_art.iterrows():
        id = row["id"]
        real_name = row["real_name"].split() 
        art_name = row["art_name"]        
        yob = str(row["year_of_birth"])+"-01-01"
        country = row["country"].replace(" ", "")
        email = row["email"]

        node = rdflib.URIRef("https://example.org/Person/{}".format(id))

        # Add artist as type Person
        kg.add(node, kg.get_ns("rdf").type, kg.get_ns("scm").Person)

        # Add person properties
        kg.add(node, kg.get_ns("scm").nationality, rdflib.Literal(country))
        kg.add(node, kg.get_ns("scm").givenName, rdflib.Literal(real_name[0]))
        kg.add(node, kg.get_ns("scm").familyName, rdflib.Literal(real_name[-1]))
        kg.add(node, kg.get_ns("scm").email, rdflib.Literal(email))
        if art_name:
            kg.add(node, kg.get_ns("scm").alternateName, rdflib.Literal(art_name))

        kg.add(node, kg.get_ns("scm").birthDate, rdflib.Literal(yob))

        # Add country node if it does not exist
        if country not in countries:
            country_node = rdflib.URIRef("https://example.org/Country/{}".format(country))
            kg.add(country_node, kg.get_ns("rdf").type, kg.get_ns("scm").Country)
            # Save country node
            countries[country] = country_node
        else:
            country_node = countries[country]

        kg.add(node, kg.get_ns("scm").birthPlace, country_node)

        # Save node
        artists[id] = node

    return 0


def add_albums_genres():
    # We add the albums and genres to the graph

    # Unique URI for Genre https://example.org/Genre/id
    # Unique URI for Album https://example.org/Music_Album/country_name

    for index, row in df_alb.iterrows():
        album_id = row["id"]
        artist_id = row["artist_id"]
        title=row["album_title"]
        year = row["year_of_pub"]
        genre = row["genre"].replace(" ","")
        num_sales = row["num_of_sales"]
        critic = row["mtv_critic"]

        node = rdflib.URIRef("http://www.example.org/Music_Album/{}".format(album_id))

        # Add album as type MusicAlbum
        kg.add(node, kg.get_ns("rdf").type, kg.get_ns("scm").MusicAlbum)

        # Add properties
        date = str(year) + "-01-01"
        kg.add(node, kg.get_ns("scm").datePublished, rdflib.Literal(date, datatype=XSD['date']))
        kg.add(node, kg.get_ns("scm").name, rdflib.Literal(title))
        kg.add(node, kg.get_ns("ex").rating, rdflib.Literal(critic, datatype=XSD.integer))
        kg.add(node, kg.get_ns("ex").albumsales, rdflib.Literal(num_sales, datatype=XSD.integer))
        kg.add(node, kg.get_ns("scm").byArtist, artists[artist_id])

        # Add genre node if it does not exist
        if genre not in countries:
            genre_node = rdflib.URIRef("https://example.org/Genre/{}".format(genre))
            kg.add(genre_node, kg.get_ns("rdf").type, kg.get_ns("ex").Genre)
            # Save genre node
            genres[genre] = genre_node
        else:
            genre_node = genres[genre_node]
        kg.add(node, kg.get_ns("scm").genre, genre_node)

        # Save albums node
        albums[album_id] = node


def save():
    s = kg.save_rdf_text(format="ttl")
    kg.save_rdf("tmp.ttl")


if __name__ == "__main__":

    add_artists_countries()

    add_albums_genres()

    save()
