import kglab

kg = kglab.KnowledgeGraph()
kg.load_rdf("./new_kg_representation.ttl", format="ttl")

VIS_STYLE = {
    "scm": {
        "color": "orange",
        "size": 20,
    },
    "ex":{
        "color": "blue",
        "size": 35,
    },
}

subgraph = kglab.SubgraphTensor(kg)
pyvis_graph = subgraph.build_pyvis_graph(notebook=True, style=VIS_STYLE)

pyvis_graph.force_atlas_2based()
pyvis_graph.show("tmp.fig01.html")