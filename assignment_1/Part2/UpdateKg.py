from rdflib import Graph, Literal, Namespace, RDF, RDFS, URIRef
from rdflib.namespace import FOAF, XSD
from SPARQLWrapper import SPARQLWrapper, JSON

# set up namespaces
ex = Namespace("https://example.org/")
schema = Namespace("https://schema.org/")

# load the existing KG in Turtle format
g = Graph()
g.parse("kg.ttl", format="ttl")

# create a SPARQL endpoint object for DBpedia
sparql = SPARQLWrapper("http://dbpedia.org/sparql")

# define the SPARQL query to retrieve birth date and birth place of each artist

# iterate over each artist in the KG and update their birth date and birth place
for artist_uri in g.subjects(RDF.type, schema.Person):
    # get the artist name from the URI
    artist_name = str(artist_uri).split("/")[-1].replace("%20", " ").replace("%26", " ")

    sparql.setQuery(f"""
                    SELECT ?person ?birthDate ?birthPlace
                    WHERE {{
                            ?person a dbo:Person ;
                            rdfs:label "{artist_name}"@en ;
                            dbo:birthDate ?birthDate ;
                            dbo:birthPlace ?birthPlace .
                            }}
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        if "birthDate" in result:
            birth_date = Literal(result["birthDate"]["value"], datatype=XSD.date)
            g.add((artist_uri, schema.birthDate, birth_date))
        if "birthPlace" in result:
            birth_place = URIRef(result["birthPlace"]["value"])
            g.add((artist_uri, schema.birthPlace, birth_place))

    # serialize the updated KG back to Turtle format
    g.serialize(destination="kg_updated.ttl", format="ttl")