Prefixes Needed
```
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ex: <http://example.org/>
PREFIX schema: <https://schema.org/>
PREFIX mo: <http://purl.org/ontology/mo/>
```
In order to open the endpoint run on terminal.
```console
$ rdflib-endpoint serve kg_updated.ttl
```
a. Return a list of artists and their names who produce songs with genres other than
"pop" and "dance pop."
```
SELECT distinct ?artist ?name 
WHERE {
  ?song a schema:MusicRecording ;
        schema:byArtist ?artist .
  ?artist a schema:Person ;
          rdfs:label ?name .
  ?Genre a mo:Genre ;
          rdfs:label ?genre_type
  FILTER(?genre_type != "pop") .
  FILTER(?genre_type != "dance pop") .
}
limit 100
```
b. Return a list of songs released in 2016 by artists born before 1990.
```
SELECT ?title ?name ?birthdate
WHERE {
  ?song a schema:MusicRecording ;
        schema:datePublished "2016"^^xsd:gYear ;
        schema:byArtist ?artist ;
        rdfs:label ?title .
  ?artist a schema:Person ;
          schema:birthDate ?birthdate ;
  		  rdfs:label ?name
  FILTER (year(?birthdate) < 1990)
}
```
C.Who is the artist that has produced the greatest number of songs?
```
SELECT ?name (COUNT(?song) AS ?count)
WHERE {
  ?song a schema:MusicRecording ;
        schema:byArtist ?artist .
  ?artist a schema:Person ;
        rdfs:label ?name
}
GROUP BY ?artist
ORDER BY DESC(?count)
LIMIT 1
```
D.Return a list of artists born in the USA, sorted by the number of songs they have
produced. (We have places not Countries) Here is with London as birthplace
```
SELECT ?artist (COUNT(?song) AS ?num_songs)
WHERE {
  ?song a schema:MusicRecording ;
        schema:datePublished ?year ;
        schema:byArtist ?artist .
  ?artist a schema:Person ;
          schema:birthPlace <http://dbpedia.org/resource/London> .
}
GROUP BY ?artist
ORDER BY DESC(?num_songs)
```

e.
```
SELECT ?artist ?name (COUNT(?song) AS ?numSongs)
WHERE {
  ?song a schema:MusicRecording ;
        rdfs:label ?label ;
        schema:byArtist ?artist .
  ?artist a schema:Person;
        rdfs:label ?name
        
  FILTER (REGEX(?label, "love", "i"))
}
GROUP BY ?artist
ORDER BY DESC(?numSongs)
```
